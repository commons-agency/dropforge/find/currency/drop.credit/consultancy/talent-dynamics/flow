# Flow

Some people are great at ideas and terrible at timing, some are fabulous at networking but often miss the detail. Flow consultancy helps organisations flow, developing profile preferences in order to improve collaboration in swarm team environments.